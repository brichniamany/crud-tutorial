import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { Produit } from '../model/Produit.model';
import { ProduitService} from '../services/produit.service';

@Component({
  selector: 'app-add-produit',
  templateUrl: './add-produit.component.html',
  styleUrls: ['./add-produit.component.css']
})
export class AddProduitComponent implements OnInit {
newProduit = new Produit();
message:string;
  constructor(private produitService: ProduitService) {

   }
   addProduit(){
       //console.log(this.newProduit);
     this.produitService.ajouterProduit(this.newProduit);
     this.message="Produit"+ this.newProduit.nomProduit+"ajouté avec success";
   }

  ngOnInit(): void {
  }

}
