import { Injectable } from '@angular/core';
import { Produit } from '../model/Produit.model';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
produits :Produit[];

produit : Produit;
  constructor() {
    this.produits=[
      { idProduit: 1, nomProduit: "pc", prixProduit: 2500, dateCreation: new Date("08/03/2012") },
      { idProduit: 2, nomProduit: "tablette", prixProduit: 1700, dateCreation: new Date("01/06/2010") },
      { idProduit: 3, nomProduit: "imprimante", prixProduit: 800, dateCreation: new Date("07/07/2017") }
    ];
   }
   listeProduit():Produit[]{
     return this.produits;
   }
   ajouterProduit(prod: Produit){
     this.produits.push(prod);
   }
  supprimerProduit(prod: Produit) {
    //supprimer le produit prod du tableau produits
    const index = this.produits.indexOf(prod, 0);
    if (index > -1) {
      this.produits.splice(index, 1);
    }
    //ou Bien
    /* this.produits.forEach((cur, index) => {
    if(prod.idProduit === cur.idProduit) {
    this.produits.splice(index, 1);
    }
    }); */
  }
  consulterProdui(id: number): Produit {
    this.produit = this.produits.find(p => p.idProduit == id);
    return this.produit;
  }
  updateProduit(p: Produit) {
    // console.log(p);
    this.supprimerProduit(p);
    this.ajouterProduit(p);
    this.trierProduits();
  }
  trierProduits() {
    this.produits = this.produits.sort((n1, n2) => {
      if (n1.idProduit > n2.idProduit) {
        return 1;
      }
      if (n1.idProduit < n2.idProduit) {
        return -1;
      }
      return 0;
    });
  }
 

}
