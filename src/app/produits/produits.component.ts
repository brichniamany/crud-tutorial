import { Component, OnInit } from '@angular/core';
import { Produit} from '../model/Produit.model';
import { ProduitService } from '../services/produit.service';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {
  produits: Produit[]; // tab de chaine des caracteres

  constructor(private produitService: ProduitService) {
   this.produits=produitService.listeProduit();
   }

  ngOnInit(): void {
  }
  supprimerProduit(p: Produit)
  {
   //console.log(p);
   let conf = confirm("Etes-vous sûr ?");
  if (conf)
  this.produitService.supprimerProduit(p);

}

}
